
#!/bin/sh

LOG_TAG="Wifi_qrct_cal"
LOG_NAME="${0}:"
n_count=5

logi ()
{
    /system/bin/log -t $LOG_TAG -p i "$LOG_NAME $@"
}

if [ ! -d /data/misc ]; then
    logi "the directory does not exist"
fi

export CLASSPATH=/system/framework/svc.jar
/system/bin/app_process /system/bin com.android.commands.svc.Svc wifi disable
if [ "$?" == "0" ]; then
    logi "wifi disable success"
else
    logi "wifi disable failed."
fi

setprop wifi.mt.status running

stop ftmdaemon
if [ "$?" == "0" ]; then
    logi "stop ftmdaemon success"
else
    logi "stop ftmdaemon failed."
fi

stop wpa_supplicant
if [ "$?" == "0" ]; then
    logi "stop wpa_supplicant success"
else
    logi "stop wpa_supplicant failed."
fi
stop p2p_supplicant
if [ "$?" == "0" ]; then
    logi "stop p2p_supplicant success"
else
    logi "stop p2p_supplicant failed."
fi
ifconfig p2p0 down
ifconfig wlan0 down

while(($n_count>0))
do
    #Wifi builtin condition check
    echo stop > /proc/wifi_built_in/wifi_start
    if [ "$?" == "0" ]; then
        logi "rmmod wlan success"
    else
        logi "rmmod wlan failed."
    fi

    #Wifi builtin condition check
     echo ftm > /proc/wifi_built_in/wifi_start
    if [ "$?" == "0" ]; then
        logi "insmod wlan success"
        n_count=0
    else
        logi "insmod wlan failed and n_count is :$n_count"
        n_count=$(($n_count-1))
        sleep 1
    fi
done

start ftmdaemon
if [ "$?" == "0" ]; then
    logi "start ftmdaemon success."
    exit 0
else
    logi "start ftmdaemon failed"
    exit 1
fi

