
#!/bin/sh

LOG_TAG="Wifi_qrct_cal"
LOG_NAME="${0}:"
n_count=5

logi ()
{
    /system/bin/log -t $LOG_TAG -p i "$LOG_NAME $@"
}

if [ ! -d /data/misc ]; then
    logi "the directory does not exist"
fi

export CLASSPATH=/system/framework/svc.jar
/system/bin/app_process /system/bin com.android.commands.svc.Svc wifi disable
if [ "$?" == "0" ]; then
    logi "wifi disable success"
else
    logi "wifi disable failed."
fi

setprop wifi.mt.status running

PID=$(ps -e | grep ftmdaemon | awk '{print $2}') 
logi "ftmdaemon PID: $PID"
if [ $PID ]; then
kill -9 $PID
if [ "$?" == "0" ]; then
    logi "stop ftmdaemon success"
else
    logi "stop ftmdaemon failed."
fi
fi


ifconfig p2p0 down
ifconfig wlan0 down

echo 5 > /sys/module/wlan/parameters/con_mode

ftmdaemon
if [ "$?" == "0" ]; then
    logi "start ftmdaemon success."
    exit 0
else
    logi "start ftmdaemon failed"
    exit 1
fi

